import argparse
import json
from config import PosCashBalanceIDs, AmtCredit
import pandas as pd


def process_data_bureau(data: dict) -> dict:
    """
    Создаёт словарь, заполненный значениями
    таблицы bureau.
    """
    evaluated_data = eval(data['data']['record']['AmtCredit'])
    return {
        'CREDIT_TYPE': data['data']['CREDIT_TYPE'],
        'SK_ID_CURR': data['data']['record']['SK_ID_CURR'],
        'SK_ID_BUREAU': data['data']['record']['SK_ID_BUREAU'],
        'CREDIT_ACTIVE': data['data']['record']['CREDIT_ACTIVE'],
        'DAYS_CREDIT': data['data']['record']['DAYS_CREDIT'],
        'CREDIT_DAY_OVERDUE': data['data']['record']['CREDIT_DAY_OVERDUE'],
        'DAYS_CREDIT_ENDDATE': data['data']['record']['DAYS_CREDIT_ENDDATE'],
        'DAYS_ENDDATE_FACT': data['data']['record']['DAYS_ENDDATE_FACT'],
        'CNT_CREDIT_PROLONG': data['data']['record']['CNT_CREDIT_PROLONG'],
        'DAYS_CREDIT_UPDATE': data['data']['record']['DAYS_CREDIT_UPDATE'],
        'CREDIT_CURRENCY': evaluated_data.CREDIT_CURRENCY,
        'AMT_CREDIT_MAX_OVERDUE': evaluated_data.AMT_CREDIT_MAX_OVERDUE,
        'AMT_CREDIT_SUM': evaluated_data.AMT_CREDIT_SUM,
        'AMT_CREDIT_SUM_DEBT': evaluated_data.AMT_CREDIT_SUM_DEBT,
        'AMT_CREDIT_SUM_LIMIT': evaluated_data.AMT_CREDIT_SUM_LIMIT,
        'AMT_CREDIT_SUM_OVERDUE': evaluated_data.AMT_CREDIT_SUM_OVERDUE,
        'AMT_ANNUITY': evaluated_data.AMT_ANNUITY
    }


def process_data_POS_CASH_balance(data: pd.DataFrame) -> pd.DataFrame:
    """
    Создаёт датафрейм, заполненный значениями
    таблицы POS_CASH_balance.
    """
    # Делаем из распаршеного json_а датафрейм
    data = pd.DataFrame(data['data'])
    # Из колонки со словарями деллаем колонки с названиями
    # ключей словаря и соответствующими значениями, удаляем изначальную колонку.
    data = normalize_column(data.reset_index(drop=True), 'records')
    # Элементы колонки - строки, делаем из них объекты
    data['PosCashBalanceIDs'] = data['PosCashBalanceIDs'].apply(lambda x: eval(x))
    # Создаём колонки с названиями из полей объекта и соответствующими им
    # значениями.
    data['SK_ID_PREV'] = data['PosCashBalanceIDs'].apply(lambda x: x.SK_ID_PREV)
    data['SK_ID_CURR'] = data['PosCashBalanceIDs'].apply(lambda x: x.SK_ID_CURR)
    data['NAME_CONTRACT_STATUS'] = data['PosCashBalanceIDs'].apply(lambda x: x.NAME_CONTRACT_STATUS)
    # Удаляем колонку, из которых брали эти объекты.
    data = data.drop(columns = 'PosCashBalanceIDs')
    # Возвращаем датафрейм, с колонками, соответствующими
    # колонкам POS_CASH_balance.
    return data


def normalize_column(df: pd.DataFrame, column: str) -> pd.DataFrame:
    """Заменяет колонку со словарем на несколько колонок.

    Имена новых колонок - ключи словаря
    Значения в новых колонках - значения по заданному ключу в словаре
    """
    return pd.concat(
        [
            df,
            pd.json_normalize(df[column]),
        ],
        axis=1
    ).drop(columns=[column])


parser = argparse.ArgumentParser(description='Logs to csv')
parser.add_argument('LOG_PATH', type=str, help='Path for logs')
parser.add_argument('CSV_PATH', type=str, help="Path where save csv'"'s')
args = parser.parse_args()


if __name__ == '__main__':
    data_bureau = []
    with open(args.LOG_PATH + '/POS_CASH_balance_plus_bureau.log') as f:
        i = 0
        for line in f:
            json_loaded = json.loads(line)
            if json_loaded["type"] == 'bureau':
                data_bureau.append(process_data_bureau(json_loaded))
            elif json_loaded["type"] == 'POS_CASH_balance':
                if i == 0:
                    data_POS_CASH_balance = process_data_POS_CASH_balance(json_loaded)
                    i += 1
                else:
                    data_POS_CASH_balance = pd.concat([data_POS_CASH_balance,
                                                       process_data_POS_CASH_balance(json_loaded)],
                                                      ignore_index=True)
    data_bureau = pd.DataFrame(data_bureau)
    data_bureau.to_csv(args.CSV_PATH + '/data_bureau.csv', index=False)
    data_POS_CASH_balance.to_csv(args.CSV_PATH + '/data_POS_CASH_balance.csv', index=False)
