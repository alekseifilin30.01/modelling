import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH
application_train = pd.read_csv(PATH + '/application_train.csv')
application_test = pd.read_csv(PATH + '/application_test.csv')
application_train_test = pd.concat([application_train,
                                    application_test], ignore_index=True)
description = pd.read_csv(PATH + '/HomeCredit_columns_description.csv',
                          encoding_errors='ignore')

# Создаём датафрейм, в который сложим наши будущие фичи
app_train_test_features = pd.DataFrame(application_train_test['SK_ID_CURR'])

# Находим все колонки с описанием того, есть ли документ
doc_columns = []
for i in range(2, 22):
    doc_columns.append(f'FLAG_DOCUMENT_{i}')

# Считаем количество документов
app_train_test_features['doc_count'] = application_train_test[doc_columns].sum(axis=1)

# Выбираем колонки с информацией о доме
desc = 'Normalized information about building where the client lives,' \
       ' What is average (_AVG suffix), modus (_MODE suffix), median ' \
       '(_MEDI suffix) apartment size, common area, living area, age of building,' \
       ' number of elevators, number of entrances, state of the building, number of floor'

building_rows = description.loc[description['Description'] == desc]['Row']

application_train_test['Home_flag'] = application_train_test[building_rows].isnull().sum(axis=1)
application_train_test.loc[application_train_test['Home_flag'] < 30, 'Home_flag'] = 1
application_train_test.loc[application_train_test['Home_flag'] >= 30, 'Home_flag'] = 0

app_train_test_features['home_flag'] = application_train_test['Home_flag']

# Вычисляем полный возраст
application_train_test['Full_Age'] = 0
application_train_test['Full_Age'] =\
    (application_train_test['DAYS_BIRTH'] * (-1) / 365).astype(int)

app_train_test_features['Full_Age'] = application_train_test['Full_Age']

# Считаем возраст смены документа
application_train_test['Doc_Change_Year'] = (((application_train_test['DAYS_BIRTH']
                                              - application_train_test['DAYS_ID_PUBLISH'])
                                             * (-1) / 365).astype(int))

app_train_test_features['Doc_Change_Year'] = application_train_test['Doc_Change_Year']

# Выставляем флаги о задержке смены документа
application_train_test['Doc_Change_Delay_Flag'] = 0
did_he_change_doc = np.logical_and(application_train_test['Full_Age'] >= 14,
                                   application_train_test['Doc_Change_Year'] < 14)\
    | np.logical_and(application_train_test['Full_Age'] >= 20, application_train_test['Doc_Change_Year'] < 20)\
    | np.logical_and(application_train_test['Full_Age'] >= 45, application_train_test['Doc_Change_Year'] < 45)
application_train_test.loc[did_he_change_doc, 'Doc_Change_Delay_Flag'] = 1

app_train_test_features['Doc_Change_Delay_Flag'] = application_train_test['Doc_Change_Delay_Flag']

app_train_test_features['Part_For_Payment'] =\
    application_train_test['AMT_ANNUITY']/application_train_test['AMT_INCOME_TOTAL']

# Вычисляем число взрослых
application_train_test['CNT_ADULTS'] = application_train_test['CNT_FAM_MEMBERS'] -\
                                       application_train_test['CNT_CHILDREN']

# Смотрим, сколько денег приходится на ребёнка и взрослого
app_train_test_features['AVG_INC_KID'] = application_train_test['CNT_CHILDREN'] * \
    application_train_test['AMT_INCOME_TOTAL'] / application_train_test['CNT_FAM_MEMBERS']
app_train_test_features['AVG_INC_ADULT'] = application_train_test['CNT_ADULTS'] * \
    application_train_test['AMT_INCOME_TOTAL'] / application_train_test['CNT_FAM_MEMBERS']

# Не придумал свои веса для внешнего источника
app_train_test_features['EXT_SOURCE_123'] = application_train_test[['EXT_SOURCE_1',
                                                                   'EXT_SOURCE_2', 'EXT_SOURCE_3']].mean(axis=1)

mean_gender_education = application_train_test.groupby(['CODE_GENDER',
                                                        'NAME_EDUCATION_TYPE'])['AMT_INCOME_TOTAL'].transform('mean')
app_train_test_features['MEAN_GENDER_AND_EDUCATION'] = (mean_gender_education -
                                                        application_train_test['AMT_INCOME_TOTAL'])

app_train_test_features.to_csv(SAVE_PATH + '/app_train_test_features.csv', index=False)
