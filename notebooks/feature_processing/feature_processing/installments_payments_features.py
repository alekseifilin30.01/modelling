import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='installments_payments features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH
installments_payments = pd.read_csv(PATH + '/installments_payments.csv')

# Считаю задержку в оплате
installments_payments['PAYMENT_DELAY'] = installments_payments['DAYS_INSTALMENT']\
    - installments_payments['DAYS_ENTRY_PAYMENT']
# Считаю максимальную, минимальную и среднюю задержку по оплатам для клиента
new_columns = installments_payments.groupby('SK_ID_CURR').agg({'PAYMENT_DELAY': ['max',
                                                                                 'min', 'mean']})['PAYMENT_DELAY']
new_columns.columns = ['PAYMENT_DELAY_max', 'PAYMENT_DELAY_min', 'PAYMENT_DELAY_mean']
installments_payments = installments_payments.merge(new_columns, on='SK_ID_CURR')
# Построчно считаю долг как разницу между тем, сколько надо заплатить и сколько клиент заплатил
installments_payments['DEBT'] = installments_payments['AMT_INSTALMENT'] - installments_payments['AMT_PAYMENT']
# Считаю общую сумму долга для клиента
debt_sum = installments_payments.groupby('SK_ID_CURR')['DEBT'].sum()
debt_sum = pd.DataFrame(debt_sum)
debt_sum.columns = ['DEBT_SUM']
installments_payments = installments_payments.merge(debt_sum, on='SK_ID_CURR')
installments_payments.to_csv(SAVE_PATH + '/installments_payments.csv', index=False)
