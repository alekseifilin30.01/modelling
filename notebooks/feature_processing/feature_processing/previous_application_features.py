import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='previous_application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH
previous_application = pd.read_csv(PATH + '/previous_application.csv')


def count_func(column, prefix, left='SK_ID_CURR', df=previous_application):
    '''
    Насчитывает значения столбца column для каждого значения left
    :param column: столбец, по которому считается количество значений
    :param prefix: префикс для новых столбцов
    :param left: по какому индексу группируем
    :param df: датафрейм с фичами для насчёта
    :return: датафрейм с новыми столбцами
    '''
    count = pd.pivot_table(
        df,
        index=left,
        columns=column,
        values='SK_ID_PREV',
        aggfunc='count',
        fill_value=0,
    ).reset_index().add_prefix(prefix)
    df = df.merge(count, left_on=left,
                  right_on=prefix+'SK_ID_CURR').drop(prefix+'SK_ID_CURR', axis=1)
    return df


# Считаю количество seller industry и contract type для одного человека
# и какие покупки он совершает на кредиты
previous_application = count_func('NAME_SELLER_INDUSTRY', 'IND_CNT_')
previous_application = count_func('NAME_CONTRACT_TYPE', 'CON_CNT_')
previous_application = count_func('NAME_GOODS_CATEGORY', 'GDS_CNT_')

max_min_columns = ['AMT_ANNUITY', 'AMT_APPLICATION', 'AMT_CREDIT', 'AMT_GOODS_PRICE']
# Вычисляю максимум и минимум по тому, сколько клиент платил за раз,
# сколько денег он просил, сколько получил  и цене того, что он хотел купить.
for columns in max_min_columns:
    new_columns = previous_application.groupby('SK_ID_CURR').agg({columns: ['max', 'min']})[columns]
    new_columns.columns = [f'{columns}_max', f'{columns}_min']
    previous_application = previous_application.merge(new_columns, on='SK_ID_CURR')

previous_application.to_csv(SAVE_PATH + '/previous_application.csv', index=False)
