import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='bureau features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH

bureau = pd.read_csv(PATH + '/bureau.csv')

# Датасет, чтобы складывать в него фичи
bureau_features = bureau[['SK_ID_CURR', 'SK_ID_BUREAU']]

# Смотрим суммы просрочки

minmax_debts = bureau.groupby('SK_ID_CURR').agg(MIN_DEBT=('AMT_CREDIT_SUM_DEBT', 'min'),
                                                MAX_DEBT=('AMT_CREDIT_SUM_DEBT', 'max'))
bureau_features = bureau_features.merge(minmax_debts, on='SK_ID_CURR', how='left')
bureau_features[['MIN_DEBT', 'MAX_DEBT']] = bureau_features[['MIN_DEBT', 'MAX_DEBT']].fillna(0)

# Смотрим долю просрочки

bureau['PART_DEBT'] = 0
bureau.loc[bureau['CREDIT_ACTIVE'] == 'Active', 'PART_DEBT'] =\
    bureau['AMT_CREDIT_SUM_DEBT']/bureau['AMT_CREDIT_SUM']

bureau_features['PART_DEBT'] = bureau['PART_DEBT'].fillna(0)

# Чтобы в дальнейшем удалить дубликаты по SK_ID_CURR, оставлю только максимальную долю просрочки

MAX_PART_DEBT = bureau_features.groupby('SK_ID_CURR').agg(MAX_PART_DEBT=('PART_DEBT', 'max'))
bureau_features = bureau_features.merge(MAX_PART_DEBT, on='SK_ID_CURR').drop('PART_DEBT', axis=1)

# Считаем типы кредитов

bureau_types = pd.pivot_table(
    bureau,
    index='SK_ID_CURR',
    columns='CREDIT_TYPE',
    values='DAYS_CREDIT_UPDATE',
    aggfunc='count',
    fill_value=0,
).reset_index().add_prefix('CNT_')

bureau_features = bureau_features.merge(bureau_types, left_on='SK_ID_CURR', right_on='CNT_SK_ID_CURR')
bureau_features = bureau_features.drop('CNT_SK_ID_CURR', axis=1)

# Смотрим, просрочен или закрыт кредит

bureau['IS_EXPIRED'] = 0
bureau.loc[bureau['CREDIT_DAY_OVERDUE'] > 1, 'IS_EXPIRED'] = 1

bureau['IS_CLOSED'] = 1
bureau.loc[pd.isna(bureau['DAYS_ENDDATE_FACT']), 'IS_CLOSED'] = 0

# Считаем количество просроченных по типу

bureau_cnt_expired = pd.pivot_table(
    bureau,
    index='SK_ID_CURR',
    columns='CREDIT_TYPE',
    values='IS_EXPIRED',
    aggfunc='sum',
    fill_value=0,
).reset_index().add_prefix('CNT_EXPIRED_')

# Считаем количество закрытых по типу

bureau_cnt_closed = pd.pivot_table(
    bureau,
    index='SK_ID_CURR',
    columns='CREDIT_TYPE',
    values='IS_CLOSED',
    aggfunc='sum',
    fill_value=0,
).reset_index().add_prefix('CNT_CLOSED_')

bureau_features = bureau_features.merge(bureau_cnt_expired, left_on='SK_ID_CURR', right_on='CNT_EXPIRED_SK_ID_CURR')
bureau_features = bureau_features.drop('CNT_EXPIRED_SK_ID_CURR', axis=1)

bureau_features = bureau_features.merge(bureau_cnt_closed, left_on='SK_ID_CURR', right_on='CNT_CLOSED_SK_ID_CURR')
bureau_features = bureau_features.drop('CNT_CLOSED_SK_ID_CURR', axis=1)

# Добавлю в датасет некоторые фичи, нагенерированные с помощь bureau_balance
bureau_balance_features = pd.read_csv(PATH + 'bureau_balance_features.csv')

columns_for_merge = ['SK_ID_BUREAU', 'DPD_0', 'DPD_1', 'DPD_2', 'DPD_3', 'DPD_4', 'DPD_5',
       'CNT_CLOSED', 'NO_INFO', 'CNT_OPEN', 'CNT_CREDITS']

# Добавил фичи с количествами кредитов, заполнил пропуски нулями, т.к. если нет информации о кредите, то нет и кредита
bureau_features = bureau_features.merge(bureau_balance_features[columns_for_merge], on='SK_ID_BUREAU', how='left')
bureau_features[columns_for_merge] = bureau_features[columns_for_merge].fillna(0)
columns_for_merge.remove('SK_ID_BUREAU')

# Посчитал количество различных кредитов у каждого человека и удалил колонки с более ненужной информацией
sum_balance_data = bureau_features.groupby('SK_ID_CURR')[columns_for_merge].sum().add_prefix('SUM_')
bureau_features = bureau_features.merge(sum_balance_data, on='SK_ID_CURR', how='left').drop(columns_for_merge, axis=1)
bureau_features = bureau_features.drop(['SK_ID_BUREAU', 'CNT_SK_ID_CURR'], axis=1)

# Удалил дубликаты по SK_ID_CURR, т.к. теперь все значения по этому столбцу уникальны для каждого клиента
bureau_features = bureau_features.drop_duplicates('SK_ID_CURR')
bureau_features = bureau_features.fillna(0)

bureau_features.to_csv(SAVE_PATH + '/bureau_features.csv', index=False)
