import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='bureau_balance features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH

bureau_balance = pd.read_csv(PATH + '/bureau_balance.csv')

# Сделал сводную таблицу с числом разных кредитов у клиента
count_status = bureau_balance.pivot_table(index='SK_ID_BUREAU', columns='STATUS',
                                          aggfunc='count').fillna(0)['MONTHS_BALANCE']
count_status['CNT_OPEN'] = count_status[['0', '1', '2', '3', '4', '5']].sum(axis=1)
count_status = count_status.rename(columns={'0': 'DPD_0', '1': 'DPD_1',
                                            '2': 'DPD_2', '3': 'DPD_3',
                                            '4': 'DPD_4', '5': 'DPD_5',
                                            'C': 'CNT_CLOSED', 'X': "NO_INFO"})

# Создал новый датафрейм, чтобы сохранять новые фичи в нём
bureau_balance_features = pd.DataFrame(bureau_balance.drop_duplicates('SK_ID_BUREAU')['SK_ID_BUREAU'])

# Добавил насчитанные фичи в основной сет с фичами
bureau_balance_features = bureau_balance_features.merge(count_status, on='SK_ID_BUREAU')

# Подсчитал и добавил количество кредитов на человека
cnt_creds = bureau_balance.groupby('SK_ID_BUREAU').agg(CNT_CREDITS= ('SK_ID_BUREAU', 'size'))
bureau_balance_features = bureau_balance_features.merge(cnt_creds, on='SK_ID_BUREAU')

# Доля открытых и закрытых кредитов у клиента
bureau_balance_features['PART_OPEN'] = bureau_balance_features['CNT_OPEN'] / bureau_balance_features['CNT_CREDITS']
bureau_balance_features['PART_CLOSED'] = bureau_balance_features['CNT_CLOSED'] / bureau_balance_features['CNT_CREDITS']


# Доля просроченных кредитов (Не придумал, как в этом случае обойтись без цикла)
for i in range(6):
    bureau_balance_features[f'PRT_{i}'] = bureau_balance_features[f'DPD_{i}'] / bureau_balance_features['CNT_CREDITS']

# Смотрю интервалы для последних открытого и закрытого кредитов (Здесь тоже не придумал, как сделать красиво)
# Выделю активные кредиты
logic_active = np.logical_and(np.logical_and(bureau_balance['STATUS'] != 'C', bureau_balance['STATUS'] != 'X'),
                              bureau_balance['MONTHS_BALANCE'] != 0)

last_active = bureau_balance.loc[logic_active].groupby('SK_ID_BUREAU')['MONTHS_BALANCE'].max()
last_active = pd.DataFrame(last_active)
last_active.columns = ['Last_Active']

# Добавляю полученную информацию в датафрейм и заполняю пропуски нулями
# (Думал заполнить каким-нибудь большим отрицательным числом, но не уверен в необходимости)
bureau_balance_features = bureau_balance_features.merge(last_active, on='SK_ID_BUREAU', how='left')
bureau_balance_features['Last_Active'] = bureau_balance_features['Last_Active'].fillna(0)

# Выделю закрытые кредиты
logic_closed = bureau_balance['STATUS'] == 'C'

last_closed = bureau_balance.loc[logic_closed].groupby('SK_ID_BUREAU')['MONTHS_BALANCE'].max()
last_closed = pd.DataFrame(last_closed)
last_closed.columns = ['Last_Closed']

bureau_balance_features = bureau_balance_features.merge(last_closed, on='SK_ID_BUREAU', how='left')
bureau_balance_features['Last_Closed'] = bureau_balance_features['Last_Closed'].fillna(0)

bureau_balance_features.to_csv(SAVE_PATH + '/bureau_balance_features.csv', index=False)
