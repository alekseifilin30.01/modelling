import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='credit_card_balance features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH

credit_card_balance = pd.read_csv(PATH + '/credit_card_balance.csv')

# Считаю количество статусов
card_statuses_flag = pd.pivot_table(
    credit_card_balance,
    index='SK_ID_CURR',
    columns='NAME_CONTRACT_STATUS',
    values='AMT_BALANCE',
    aggfunc='count',
    fill_value=0,
).reset_index().add_prefix('CNT_')

credit_card_balance = credit_card_balance.merge(card_statuses_flag,
                                                left_on='SK_ID_CURR', right_on='CNT_SK_ID_CURR')
credit_card_balance = credit_card_balance.drop('CNT_SK_ID_CURR', axis=1)

# Соберу колонки для подсчёта аггрегатов
columns_for_agregate = ['MONTHS_BALANCE', 'AMT_BALANCE', 'AMT_CREDIT_LIMIT_ACTUAL']

# Вычисляю аггрегаты
agg_columns = credit_card_balance.groupby('SK_ID_CURR')[columns_for_agregate].agg(['mean', 'min', 'max'])
agg_columns.columns = ['_'.join(col) for col in agg_columns.columns.values]

# Беру аггрегаты за последние 3 месяца
agg_columns_3_month = (credit_card_balance
               .loc[credit_card_balance['MONTHS_BALANCE'] < 3]
               .groupby('SK_ID_CURR')[columns_for_agregate]
               .agg(['mean', 'min', 'max']))
agg_columns_3_month.columns = ['_'.join(col) for col in agg_columns_3_month.columns.values]

# Вычисляю разницу между аггрегатами за последние три месяца и за всё время
agg_columns_diff = agg_columns - agg_columns_3_month
agg_columns_diff = agg_columns_diff.add_suffix('_3_month_diff')

agg_columns_3_month = agg_columns_3_month.add_suffix('_3_month')

credit_card_balance = credit_card_balance.merge(agg_columns, on='SK_ID_CURR')
credit_card_balance = credit_card_balance.merge(agg_columns_3_month, on='SK_ID_CURR')
credit_card_balance = credit_card_balance.merge(agg_columns_diff, on='SK_ID_CURR')

credit_card_balance.to_csv(SAVE_PATH + '/credit_card_balance_features.csv', index=False)
