COPY public.pos_cash_balance(
	"SK_ID_PREV", "SK_ID_CURR", "MONTHS_BALANCE", "CNT_INSTALMENT", "CNT_INSTALMENT_FUTURE", "NAME_CONTRACT_STATUS", "SK_DPD", "SK_DPD_DEF")
FROM 'E:\GitLab_CFT\Modelling\raw_data\home-credit\pos_cash_balance.csv' DELIMITER ',' CSV HEADER;