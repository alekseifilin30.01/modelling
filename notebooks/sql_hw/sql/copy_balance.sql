create table bureau_balance (
    SK_ID_BUREAU integer,
    MONTHS_BALANCE integer,
    STATUS varchar(128)  
);

COPY bureau_balance(
    SK_ID_BUREAU,
    MONTHS_BALANCE,
    STATUS
) 
FROM 'E:\GitLab_CFT\Modelling\raw_data\home-credit\bureau_balance.csv' DELIMITER ',' CSV HEADER;