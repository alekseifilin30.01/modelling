COPY public.installments_payments(
	"SK_ID_PREV", "SK_ID_CURR", "NUM_INSTALMENT_VERSION", "NUM_INSTALMENT_NUMBER", "DAYS_INSTALMENT", "DAYS_ENTRY_PAYMENT", "AMT_INSTALMENT", "AMT_PAYMENT")
FROM 'E:\GitLab_CFT\Modelling\raw_data\home-credit\installments_payments.csv' DELIMITER ',' CSV HEADER;