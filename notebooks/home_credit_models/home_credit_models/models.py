import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

from catboost import CatBoostClassifier, Pool, metrics, cv

from sklearn.preprocessing import StandardScaler

from sklearn.model_selection import train_test_split

from sklearn.metrics import roc_auc_score

from sklearn.model_selection import KFold

import pickle

import argparse

parser = argparse.ArgumentParser(description='application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
SAVE_PATH = args.SAVE_PATH

application_df = pd.read_csv(PATH + 'application.csv')
bureau_df = pd.read_csv(PATH + 'bureau.csv')
credit_card_df = pd.read_csv(PATH + 'credit_card.csv')
installment_payments_df = pd.read_csv(PATH + 'installments_payments.csv')
previous_application_df = pd.read_csv(PATH + 'previous_application.csv')

# Заполню пока ещё не заполненные пропуски
median_exit = application_df[application_df['EXT_SOURCE_123'].notnull()]['EXT_SOURCE_123'].median()
application_df['EXT_SOURCE_123'] = application_df['EXT_SOURCE_123'].fillna(median_exit)

median_payment = application_df[application_df['Part_For_Payment'].notnull()]['Part_For_Payment'].median()
application_df['Part_For_Payment'] = application_df['Part_For_Payment'].fillna(median_payment)

application_df['CNT_ADULTS'] = application_df['CNT_ADULTS'].fillna(1)

application_df['AVG_INC_KID'] = application_df['AVG_INC_KID'].fillna(0)

application_df['AVG_INC_ADULT'] = application_df['AVG_INC_ADULT'].fillna(1)

# Тут возникали inf'ы
bureau_df['PART_DEBT'] = bureau_df['PART_DEBT'].clip(0, 10000)

# Загрузим и смёржим данные
train_df = application_df.loc[application_df['TARGET'].notna()]
test_df = application_df.loc[application_df['TARGET'].isna()]

train_df = train_df.merge(bureau_df.drop('TARGET', axis=1), on='SK_ID_CURR')
train_df = train_df.merge(credit_card_df.drop('TARGET', axis=1), on='SK_ID_CURR')
train_df = train_df.merge(installment_payments_df.drop('TARGET', axis=1), on='SK_ID_CURR')
train_df = train_df.merge(previous_application_df.drop('TARGET', axis=1), on='SK_ID_CURR')

# Разделим данные на признаки и метки
X = train_df.iloc[:, 2:]
y = train_df.iloc[:, 1]

# Найдём признаки, которые регрессия посчитала не нужными

bool_cols = [col for col in X
             if X[[col]].dropna().isin([0, 1]).all().values]
numeric_cols = list(set(X.columns) - set(bool_cols))

logreg = LogisticRegression(penalty='l1', C=1, solver='liblinear')
logreg.fit(X, y)

# Выберем признаки, которые модель посчитала достаточно значимыми
nparray_coeffs = np.array(logreg.coef_)

non_zero_coefs = np.where(abs(nparray_coeffs[0]) >= 1e-6)[0]
X_lr = X[X.columns[non_zero_coefs]]

# Заново соберём числовые и бинарные признаки, т.к. их количество изменилось
bool_cols = [col for col in X_lr
             if X_lr[[col]].dropna().isin([0, 1]).all().values]
numeric_cols = list(set(X_lr.columns) - set(bool_cols))

# Проведём кросс-валидацию на пяти фолдах

logloss_score = []
kf = KFold(n_splits=5, shuffle=False)
for train_index, valid_index in kf.split(X_lr):
    train = X_lr.iloc[train_index].copy()
    valid = X_lr.iloc[valid_index].copy()
    y_train = y.iloc[train_index].copy()
    y_valid = y.iloc[valid_index].copy()

    X_train_bool = train[bool_cols]
    X_valid_bool = valid[bool_cols]

    scaler = StandardScaler()
    X_train_scaled = scaler.fit_transform(train[numeric_cols])
    X_valid_scaled = scaler.transform(valid[numeric_cols])

    X_train_preprocessed = np.concatenate((X_train_scaled, X_train_bool), axis=1)
    X_valid_preprocessed = np.concatenate((X_valid_scaled, X_valid_bool), axis=1)

    logloss_clf = LogisticRegression(penalty='l2', C=1e-2)
    logloss_clf.fit(X_train_preprocessed, y_train)
    y_pred_proba = logloss_clf.predict_proba(X_valid_preprocessed)[:, 1]

    logloss_auc = round(roc_auc_score(y_valid, y_pred_proba), 4)
    logloss_score.append(logloss_auc)

print('LogLoss on 5 folds: ', logloss_score, 'Mean: ', round(np.mean(logloss_score), 4), 'Std: ',
      round(np.std(logloss_score), 4))

LogReg = LogisticRegression(penalty='l2', C=1e-2)
LogReg.fit(X_lr, y)

pkl_filename = SAVE_PATH + 'LogReg.pickle'
with open(pkl_filename, 'wb') as file:
    pickle.dump(LogReg, file)

X_train, X_validation, y_train, y_validation = train_test_split(X, y, train_size=0.75, random_state=42)

model = CatBoostClassifier(
    custom_loss=[metrics.AUC()],
    random_seed=42,
    logging_level='Silent'
)

model.fit(
    X_train, y_train,
    eval_set=(X_validation, y_validation),
    plot=True
)

cv_params = model.get_params()
cv_params.update({
    'loss_function': metrics.Logloss()
})
cv_data = cv(
    Pool(X, y),
    cv_params,
    plot=True
)

print('Best validation AUC score: {:.2f}±{:.2f} on step {}'.format(
    np.max(cv_data['test-AUC-mean']),
    cv_data['test-AUC-std'][np.argmax(cv_data['test-AUC-mean'])],
    np.argmax(cv_data['test-AUC-mean'])
))

CatBoostClassifier.save_model(model, SAVE_PATH + 'CatBoost', format="python ")
