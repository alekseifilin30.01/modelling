import numpy as np
import pandas as pd
import argparse
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, OrdinalEncoder
from category_encoders import TargetEncoder

import matplotlib.pyplot as plt
import seaborn as sns


parser = argparse.ArgumentParser(description='application processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

application_train_df = pd.read_csv(args.PATH + '/application_train.csv')
description = pd.read_csv(args.PATH + '/HomeCredit_columns_description.csv', encoding_errors='ignore')


def column_explain(column, df = application_train_df, left = -1, right = 50, step = 5):
    '''
    Выводит информацию о колонке, её описание,
    строит боксплот и показывает, какие значения
    принимает таргет в зависимости от значений в колонке
    df - датафрейм с признаками
    column - колонка, по которой хотим получить информацию
    left, right, step - границы интервала и шаг, по которому смотрим на фичи
    '''
    print(description.loc[description['Row'] == column]['Description'])
    plt.figure(figsize=(15, 5))
    sns.boxplot(x=df[column])
    df['Column_check'] = pd.cut(df[column], list(range(left, right, step)))
    # Как ведет себя таргет в заисимости от значений признака
    print(df.groupby('Column_check')['TARGET'].agg(['mean','sem','count']))
    print("Nans count", df[column].isnull().sum())


def get_outliers_info(df, d=1.5):
    """
    Статистика по наличию выбросов в признаках.
    df - датасет с признаками
    d - коэффициент, на который умножается IQR (обычно 1.5, что соотв. усам на boxplot)
    """
    df = df.copy()
    q75 = df.quantile(0.75)
    q25 = df.quantile(0.25)
    iqr = q75 - q25

    feature_list, outlier_list = [], []
    numerical_columns = [c for c in df.columns if df[c].dtype in ['float64', 'int64'] and df[c].nunique() > 10]

    for feature in numerical_columns:
        lower_threshold = q25.loc[feature] - d * iqr[feature]
        upper_threshold = q75.loc[feature] + d * iqr[feature]

        df['is_outlier'] = np.nan
        df.loc[(df[feature] < lower_threshold) | (df[feature] > upper_threshold), 'is_outlier'] = 1
        df['is_outlier'] = df['is_outlier'].fillna(0)

        feature_list.append(feature)
        outlier_list.append(df['is_outlier'].mean())

    outliers_df = pd.DataFrame(index=feature_list, data=outlier_list, columns=['ratio_outliers'])
    outliers_df = outliers_df[outliers_df['ratio_outliers'] > 0]
    outliers_df = outliers_df.sort_values(by='ratio_outliers', ascending=False)

    return outliers_df


def fill_median(column, df = application_train_df):
    '''
    Заполняет наны столбца column медианой
    Если не получилось вернёт изначальный столбец
    '''
    df_median = df.copy()
    try:
        median = df_median.loc[df_median[column].notnull()][column].median()
        df_median[column] = df_median[column].fillna(median)
        return df_median[column]
    except:
        return df[column]


def encoding(df, columns):
    '''
    Обучает OHE и присоединяет фичи к датафрейму, удаляя те, на основе которых строился OHE
    '''
    enc = OneHotEncoder(sparse=False)
    categorical_features_enc = enc.fit_transform(df[columns])
    categorical_features_enc = pd.DataFrame(data=categorical_features_enc,
                                            columns=enc.get_feature_names_out(columns))
    df = df.drop(columns, axis=1)
    df = pd.concat([df, categorical_features_enc], axis=1)
    return df


def get_nan_info(df):
    """
    Статистика по наличию пропусков в признаках.
    df - датасет с признаками
    """
    df = df.copy()
    nans_df = pd.DataFrame(df.isnull().sum().rename('n_nans'))

    N = df.shape[0]
    nans_df['ratio_nans'] = nans_df['n_nans'] / N
    nans_df['ratio_nans'] = nans_df['ratio_nans'].round(2)

    nans_df = nans_df[nans_df['n_nans'] > 0]
    nans_df = nans_df.sort_values(by='n_nans', ascending=False)

    for feature in nans_df.index:
        nans_df.loc[feature, 'nan_target_mean'] = df[df[feature].isnull()]['TARGET'].mean()
        nans_df.loc[feature, 'not_nan_target_mean'] = df[df[feature].notnull()]['TARGET'].mean()

    return nans_df


def make_nan(df, column, value):
    """
    Возвращает колонку, в которой значения больше value становятся nan
    df - датасет с признаками
    column - колонка, в которой нужно убрать признаки,
    которые больше, чем value
    """
    nan_df = df.copy()
    nan_df.loc[nan_df[column] > value, [column]] = np.nan
    return nan_df[column]


# Найдем все категориальные признаки
categorical_columns = [c for c in application_train_df.columns if application_train_df[c].dtype in ['object']]

# Заполним пропуски самыми простым способом: наиболее частотной категорией
for col in categorical_columns:
    mode = application_train_df[col].mode().iloc[0]
    application_train_df[col] = application_train_df[col].fillna(mode)

application_train_df = encoding(application_train_df, categorical_columns)

application_train_df.loc[application_train_df['DAYS_EMPLOYED'] > 0, 'DAYS_EMPLOYED'] = np.nan

# DAYS_EMPLOYED
DAYS_EMPLOYED_median = application_train_df[application_train_df['DAYS_EMPLOYED'].notnull()]['DAYS_EMPLOYED'].median()
application_train_df['DAYS_EMPLOYED'] = application_train_df['DAYS_EMPLOYED'].fillna(DAYS_EMPLOYED_median)

# AMT_REQ_CREDIT_BUREAU_QRT
application_train_df.loc[application_train_df['AMT_REQ_CREDIT_BUREAU_QRT'] > 8, ['AMT_REQ_CREDIT_BUREAU_QRT']] = np.nan
application_train_df['AMT_REQ_CREDIT_BUREAU_QRT'] = application_train_df['AMT_REQ_CREDIT_BUREAU_QRT'].fillna(0)
# AMT_REQ_CREDIT_BUREAU_MON
application_train_df['AMT_REQ_CREDIT_BUREAU_MON'] = application_train_df['AMT_REQ_CREDIT_BUREAU_MON'].fillna(0)
# OBS_N_CNT_SOCIAL_CIRCLE
application_train_df['OBS_30_CNT_SOCIAL_CIRCLE'] = application_train_df['OBS_30_CNT_SOCIAL_CIRCLE'].fillna(0)
application_train_df['OBS_60_CNT_SOCIAL_CIRCLE'] = make_nan(application_train_df, 'OBS_60_CNT_SOCIAL_CIRCLE', 34)
application_train_df['OBS_60_CNT_SOCIAL_CIRCLE'] = application_train_df['OBS_60_CNT_SOCIAL_CIRCLE'].fillna(0)
# AMT_GOODS_PRICE
median = application_train_df.loc[application_train_df['AMT_GOODS_PRICE'].notnull()]['AMT_GOODS_PRICE'].median()
application_train_df['AMT_GOODS_PRICE'] = application_train_df['AMT_GOODS_PRICE'].fillna(median)
# AMT_INCOME_TOTAL
application_train_df['AMT_INCOME_TOTAL'] = application_train_df['AMT_INCOME_TOTAL'].clip(0, 1000000)
# DAYS_LAST_PHONE_CHANGE
application_train_df['DAYS_LAST_PHONE_CHANGE'] = application_train_df['DAYS_LAST_PHONE_CHANGE'].fillna(0)
# CNT_FAM_MEMBERS
application_train_df['CNT_FAM_MEMBERS'] = application_train_df['CNT_FAM_MEMBERS'].fillna(1)
# AMT_ANNUITY
application_train_df['AMT_ANNUITY'] = fill_median('AMT_ANNUITY')
# DEF_30_CNT_SOCIAL_CIRCLE
application_train_df['DEF_30_CNT_SOCIAL_CIRCLE'] = application_train_df['DEF_30_CNT_SOCIAL_CIRCLE'].fillna(0)
# DEF_60_CNT_SOCIAL_CIRCLE
application_train_df['DEF_60_CNT_SOCIAL_CIRCLE'] = application_train_df['DEF_60_CNT_SOCIAL_CIRCLE'].fillna(0)
desc = 'Normalized information about building where the client lives, What is average (_AVG suffix),' \
       ' modus (_MODE suffix), median (_MEDI suffix) apartment size, common area, living area, age of building,' \
       ' number of elevators, number of entrances, state of the building, number of floor'
building_rows = description.loc[description['Description'] == desc]['Row']
# Не придумал что делать с этим огромным количеством признаков
# с характеристиками жилья, поэтому просто заполнил пропуски медианой
# проверил некоторые из признаков, особых аномалий не нашёл
# Категориальные признаки он сделать не может, поэтому я их пропускаю
for rows in building_rows:
    application_train_df[rows] = fill_median(rows)
# AMT_REQ_CREDIT_BUREAU_YEAR
application_train_df['AMT_REQ_CREDIT_BUREAU_YEAR'] = application_train_df['AMT_REQ_CREDIT_BUREAU_YEAR'].fillna(0)
# Посчитал, что если количество запросов NaN, то этих запросов и не было,
# поэтому заполнил нулями
amt_columns = ['AMT_REQ_CREDIT_BUREAU_YEAR',
               'AMT_REQ_CREDIT_BUREAU_WEEK',
               'AMT_REQ_CREDIT_BUREAU_DAY',
               'AMT_REQ_CREDIT_BUREAU_HOUR']
for columns in amt_columns:
    application_train_df[columns] = application_train_df[columns].fillna(0)
# Описывается как скор из внешнего источника, не знаю что за признак, поэтому заполню медианой
sources = ['EXT_SOURCE_1', 'EXT_SOURCE_2', 'EXT_SOURCE_3']
for columns in sources:
    application_train_df[columns] = fill_median(columns)
application_train_df['OWN_CAR_AGE'] = fill_median('OWN_CAR_AGE')
application_train_df.to_csv(args.SAVE_PATH + '/application_train_df.csv', index=False)
