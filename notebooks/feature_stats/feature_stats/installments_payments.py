import pandas as pd
from scipy.stats import mannwhitneyu
import argparse


def is_significant(df, sig_column):
    '''
    Проверяет признак по критерию Манна-Уитни, если признак не значим,
    то он удаляется из датафрейма
    '''
    _, p_mw = mannwhitneyu(df[df['TARGET'] == 0][sig_column], df[df['TARGET'] == 1][sig_column])
    if p_mw >= 0.05:
        df = df.drop(sig_column, axis=1)
    return df


parser = argparse.ArgumentParser(description='application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('PATH_TO_TRAIN', type=str, help='Path to train for add target in data')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()


PATH = args.PATH
PATH_TO_TRAIN = args.PATH_TO_TRAIN
SAVE_PATH = args.SAVE_PATH

# Считаем колонки с айди и таргетом
id_target = pd.read_csv(PATH_TO_TRAIN, usecols=['SK_ID_CURR', 'TARGET'])

# Считывание данных и удаление строк по дубликатам в айди
installments_payments = pd.read_csv(PATH)
installments_payments = installments_payments.merge(id_target, on='SK_ID_CURR')
installments_payments = installments_payments.drop_duplicates('SK_ID_CURR')

# Выбор столбцов для проверки
columns_to_check = [installments_payments.columns[1]] + installments_payments.columns[9:12].tolist()\
                   + installments_payments.columns[13:15].tolist()

# Будем работать только с теми фичами, которые однозначны для каждого клиента
installments_payments = installments_payments[columns_to_check]

# Заполним пропуски
installments_payments['PAYMENT_DELAY_max'] = installments_payments['PAYMENT_DELAY_max'].fillna(0)
installments_payments['PAYMENT_DELAY_min'] = installments_payments['PAYMENT_DELAY_min'].fillna(0)
installments_payments['PAYMENT_DELAY_mean'] = installments_payments['PAYMENT_DELAY_mean'].fillna(0)

# Проверим признаки на значимость
for column in columns_to_check[1:-1]:
    installments_payments = is_significant(installments_payments, column)

# Сохраним полученый датасет
installments_payments.to_csv(SAVE_PATH, index=False)
