import pandas as pd
from sklearn.preprocessing import OrdinalEncoder
from scipy.stats import mannwhitneyu, chi2_contingency
from scipy.stats import chi2
import numpy as np
import argparse


def encoding(df, oe_column):
    '''
    Сортирует фичи по влиянию на таргет и обучает OE для энкодинга
    '''
    df_encoded = df.copy()
    value = list(map(str, df_encoded.groupby(oe_column)['TARGET'].mean().sort_values().index.values))
    oe = OrdinalEncoder(categories=[value])
    df_encoded[oe_column] = oe.fit_transform(df_encoded[[oe_column]]).astype('int')
    return df_encoded[oe_column]


def is_significant(df, sig_column):
    '''
    Проверяет признак по критерию Манна-Уитни, если признак не значим,
    то он удаляется из датафрейма
    '''
    _, p_mw = mannwhitneyu(df[df['TARGET' ]==0][sig_column], df[df['TARGET' ]==1][sig_column])
    if p_mw >= 0.05:
        df = df.drop(sig_column, axis=1)
    return df


def fill_median(medi_column, df):
    '''
    Заполняет наны столбца column медианой
    Если не получилось вернёт изначальный столбец
    '''
    df_median = df.copy()
    try:
        median = df_median.loc[df_median[medi_column].notnull()][medi_column].median()
        df_median[medi_column] = df_median[medi_column].fillna(median)
        return df_median[medi_column]
    except:
        return df[medi_column]


def chi2_significant(df, is_sign_column):
    cross_tab = pd.concat([
            pd.crosstab(df[is_sign_column], df['TARGET'], margins=False),
            df.groupby(is_sign_column)['TARGET'].agg(['count', 'mean']).round(4)
        ], axis=1).rename(columns={0: f"label=0", 1: f"label=1", "mean": 'probability_of_default'})
    cross_tab['probability_of_default'] = np.round(cross_tab['probability_of_default'] * 100, 2)
    chi2_stat, p, dof, expected = chi2_contingency(cross_tab.values)
    prob = 0.95
    critical = chi2.ppf(prob, dof)
    if abs(chi2_stat) < critical:
        df = df.drop(is_sign_column, axis=1)
    return df


parser = argparse.ArgumentParser(description='application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('PATH_TO_TRAIN', type=str, help='Path to train for add target in data')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()


PATH = args.PATH
PATH_TO_TRAIN = args.PATH_TO_TRAIN
SAVE_PATH = args.SAVE_PATH

application_train_test_features = pd.read_csv(PATH)
id_target = pd.read_csv(PATH_TO_TRAIN, usecols=['SK_ID_CURR', 'TARGET'])
# Разделяю на трейн и тест
application_train_features, application_test_features = \
    application_train_test_features[application_train_test_features['TARGET'].notna()],\
    application_train_test_features[application_train_test_features['TARGET'].isna()]


# Найдем все категориальные и бинарные признаки
categorical_columns = [c for c in application_train_features.columns if application_train_features[c].dtype in ['object']]
bool_cols = [col for col in application_train_features if application_train_features[col].dropna().value_counts().index.isin([0,1]).all()]
bool_cols = list(set(bool_cols) - {'TARGET'})

# Определим не категориальные и не бинарные признаки
non_categorical_columns = list(set(application_train_features.columns.tolist())
                               - set(bool_cols) - set(categorical_columns) - {'TARGET', 'SK_ID_CURR'})

# Заполним пропуски самыми простым способом: наиболее частотной категорией
for col in categorical_columns:
    mode = application_train_features[col].mode().iloc[0]
    application_train_features[col] = application_train_features[col].fillna(mode)

# Обучим энкодер и сразу проверим значимость признака по хи-квадрат
for col in categorical_columns:
    application_train_features[col] = encoding(application_train_features, col)
    application_train_features = chi2_significant(application_train_features, col)

# Заполню пропуски
application_train_features['EXT_SOURCE_123'] = fill_median('EXT_SOURCE_123', application_train_features)
application_train_features['Part_For_Payment'] = application_train_features['Part_For_Payment'].fillna(0)
application_train_features['CNT_ADULTS'] = application_train_features['CNT_ADULTS'].fillna(1)
application_train_features['AVG_INC_KID'] = application_train_features['AVG_INC_KID'].fillna(0)
application_train_features['AVG_INC_ADULT'] = application_train_features['AVG_INC_ADULT'].fillna(0)

# Проверяю все бинарные колонки через критерий Хи-квадрат
for column in bool_cols:
    application_train_features = chi2_significant(application_train_features, column)

# Проверяю все не категориальные колонки через критерий Манна-Уитни
for column in non_categorical_columns:
    application_train_features = is_significant(application_train_features, column)

application_train_test_features = application_train_test_features[application_train_features.columns]

# Сохраним полученый датасет
application_train_test_features.to_csv(SAVE_PATH, index=False)
