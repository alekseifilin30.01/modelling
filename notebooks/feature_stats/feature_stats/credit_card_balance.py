import pandas as pd
from scipy.stats import mannwhitneyu
import argparse


def is_significant(df, sig_column):
    '''
    Проверяет признак по критерию Манна-Уитни, если признак не значим,
    то он удаляется из датафрейма
    '''
    _, p_mw = mannwhitneyu(df[df['TARGET'] == 0][sig_column], df[df['TARGET'] == 1][sig_column])
    if p_mw >= 0.05:
        df = df.drop(sig_column, axis=1)
    return df


parser = argparse.ArgumentParser(description='application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('PATH_TO_TRAIN', type=str, help='Path to train for add target in data')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()

PATH = args.PATH
PATH_TO_TRAIN = args.PATH_TO_TRAIN
SAVE_PATH = args.SAVE_PATH
# Считаем колонки с айди и таргетом
id_target = pd.read_csv(PATH_TO_TRAIN, usecols=['SK_ID_CURR', 'TARGET'])

credit_card_balance = pd.read_csv(PATH)
credit_card_balance = credit_card_balance.merge(id_target, on='SK_ID_CURR')
columns_to_check = credit_card_balance.columns[1:2].tolist() + credit_card_balance.columns[23:].tolist()
credit_card_balance = credit_card_balance[columns_to_check].drop_duplicates('SK_ID_CURR')
# Проверю значимость признаков
for column in columns_to_check[1:-1]:
    credit_card_balance = is_significant(credit_card_balance, column)

# Сохраним полученый датасет
credit_card_balance.to_csv(SAVE_PATH, index=False)
