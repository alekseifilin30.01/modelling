import pandas as pd
from scipy.stats import mannwhitneyu
import argparse


def is_significant(df, sig_column):
    '''
    Проверяет признак по критерию Манна-Уитни, если признак не значим,
    то он удаляется из датафрейма
    '''
    _, p_mw = mannwhitneyu(df[df['TARGET' ]==0][sig_column], df[df['TARGET' ]==1][sig_column])
    if p_mw >= 0.05:
        df = df.drop(sig_column, axis=1)
    return df


parser = argparse.ArgumentParser(description='application features processing')
parser.add_argument('PATH', type=str, help='Path to file')
parser.add_argument('PATH_TO_TRAIN', type=str, help='Path to train for add target in data')
parser.add_argument('SAVE_PATH', type=str, help='Path to save')
args = parser.parse_args()


PATH = args.PATH
PATH_TO_TRAIN = args.PATH_TO_TRAIN
SAVE_PATH = args.SAVE_PATH
# Считаем колонки с айди и таргетом
id_target = pd.read_csv(PATH_TO_TRAIN, usecols=['SK_ID_CURR', 'TARGET'])

previous_application = pd.read_csv(PATH)
previous_application = previous_application.merge(id_target, on='SK_ID_CURR')

columns_to_check = previous_application.columns[1:2].tolist() + previous_application.columns[37:].tolist()
previous_application = previous_application[columns_to_check].drop_duplicates('SK_ID_CURR')
# Заполню пропуски
previous_application['AMT_GOODS_PRICE_max'] = previous_application['AMT_GOODS_PRICE_max'].fillna(0)
previous_application['AMT_GOODS_PRICE_min'] = previous_application['AMT_GOODS_PRICE_min'].fillna(0)
previous_application['AMT_ANNUITY_max'] = previous_application['AMT_ANNUITY_max'].fillna(0)
previous_application['AMT_ANNUITY_min'] = previous_application['AMT_ANNUITY_min'].fillna(0)

# Проверка значимых признаков и дроп незначимых
for column in columns_to_check[1:-1]:
    previous_application = is_significant(previous_application, column)

# Сохраним полученый датасет
previous_application.to_csv(SAVE_PATH, index=False)
